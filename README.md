# Powershell Enter VS Dev Environment

> Maintained at <https://gitlab.freedesktop.org/ryan.pavlik/powershell-enter-vsdevenv>

<!--
Copyright 2022, Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
-->

Ever find it annoying that the "Developer Powershell" provided by Visual Studio
on Windows doesn't have your customizations? It turns out that Microsoft
actually has a Powershell module to help solve this problem, providing the
cmdlet Enter-VsDevShell. However, it doesn't get installed in a auto-discovered
place. You need to import it by full path, which is almost as much of a bother
to the development workflow.

This module is very simple: It mostly just wraps using `vswhere` to find Visual
Studio, import that fancier module, and call `Enter-VsDevShell`.

## Installation

Place the `Enter-VsDevEnv` directory whole inside your
`Documents\Powershell\Modules` directory, to make it available by default.

If you install a newer version of `vswhere` e.g. from
[Scoop](https://scoop.sh), this module will prefer it over the one installed to
the "well-known" path in `Program Files (x86)`.

## Use

In Powershell (5+, preferably core/7+), run `Enter-VsDevEnv`. This will find
your newest Visual Studio install and add the correct environment variables to
build for `amd64`, on an `amd64` host, to the environment for your current
terminal session.

Available arguments let you change the VS version (by version `-Version` or year
`-Year`), change the target architecture `-Arch`, or change the host
architecture `HostArch`. These get passed to the `Enter-VsDevShell` invocation
to simply and easily add the compilation environment to any Powershell session.

## License

Code is BSL-1.0, docs are CC-BY-4.0. This project is compliant with the
[REUSE](https://reuse.software) specification.

## Contributing

This project, while more-or-less complete already, welcomes contributions.

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms.

We follow the standard freedesktop.org code of conduct,
available at <https://www.freedesktop.org/wiki/CodeOfConduct/>,
which is based on the [Contributor Covenant](https://www.contributor-covenant.org).
