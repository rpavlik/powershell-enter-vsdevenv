# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

# Original Author: Ryan Pavlik <ryan.pavlik@collabora.com>

# Maintained at https://gitlab.freedesktop.org/ryan.pavlik/powershell-enter-vsdevenv

@{

    RootModule        = 'Enter-VsDevEnv.psm1'

    ModuleVersion     = '1.0.0'

    GUID              = '769C2C8F-8C19-4D50-9AFE-C4A88B0E0454'

    Author            = 'Ryan Pavlik'

    CompanyName       = 'Collabora, Ltd.'

    Copyright         = '(c) 2022 Collabora, Ltd, licensed under the BSL-1.0 license.'

    Description       = 'Find and use MSVC, wrapping the Microsoft-provided utilities and cmdlets'

    PowerShellVersion = '5.0'
}
